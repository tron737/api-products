<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nameArr = [
            'Футболка с гербом СССР, Макс-Экстрим',
            'Футболка, Oodji',
            'Футболка, 2 шт., Oodji',
            'Футболка Гладиатор, Макс-Экстрим',
            'Футболка, MF',
            'Футболка Охота, Макс-Экстрим',
            'Футболка-поло, Envy Lab',
            'Футболка поло, U.S. Polo Assn.',
            'Футболка, U.S. Polo Assn.',
        ];

        foreach ($nameArr as $name) {
            $item = new \App\API\Items();
            $item->name = $name;
            $item->save();

            $category = \App\API\Categories::orderByRaw("RAND()")->first();

            $itemCategory = new \App\API\ItemCategory();
            $itemCategory->item_id = $item->id;
            $itemCategory->category_id = $category->id;
            $itemCategory->save();
        }
    }
}

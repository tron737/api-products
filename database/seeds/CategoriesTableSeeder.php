<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $namesArr = [
            'Брюки и шорты',
            'Рубашки',
            'Джинсы',
            'Верхняя одежда',
            'Джемперы и кардиганы',
            'Лонгсливы',
            'Толстовки',
            'Худи',
            'Костюмы',
            'Пиджаки и джакеты',
        ];
        foreach ($namesArr as $name) {
            $categories = new \App\API\Categories();
            $categories->name = $name;
            $categories->save();
        }
    }
}

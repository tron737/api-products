<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/get', function () {
    return ['item' => 'test'];
});

Route::post('auth', 'API\UserController@auth');
Route::get('categories/{categories?}', 'API\CategoriesController@index');
Route::get('items/{items?}', 'API\ItemsController@index');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('categories/new', 'API\CategoriesController@create');
    Route::put('categories/edit/{categories}', 'API\CategoriesController@update');
    Route::delete('categories/remove/{categories}', 'API\CategoriesController@destroy');

    Route::post('items/new', 'API\ItemsController@create');
    Route::put('items/edit/{items}', 'API\ItemsController@update');
    Route::delete('items/remove/{items}', 'API\ItemsController@destroy');
});
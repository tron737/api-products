Установка<br>
1. composer install<br>
2. Настроить подключение к бд в .env<br>
3. php artisan make:auth<br>
4. php artisan migrate<br>
5. php artisan passport:install<br>
6. php artisan db:seed<br>

Команда для создания пользователя 
php artisan reguser:new NAME EMAIL PASSWORD<br>
Пример<br>
php artisan reguser:new Tes1t test111@test.ru test

Методы апи

**POST /api/auth** Авторизация<br>
Поля<br>
-email<br>
-password

Пример ответа

{
    "code": true,
    "response": {
        "token": "TOKEN"
    }
}

**GET /api/categories** Получение всех категорий<br>
**POST /api/categories/new** Создание новой категории<br>
Принимаемые параметры 
-name (string)<br>
Пример ответа<br>
{
    "code": true,
    "response": {
        "id": 11
    }
}
<br>
**PUT /api/categories/edit/ID** Изменение категории<br>
Принимаемые параметры 

-name (string)<br>
**DELETE /api/categories/remove/ID** Удаление категории<br>


**GET /api/items** Получение всех товаров<br>
Принимаемые параметры 

-page (int) страница<br>
**POST /api/items/new** Создание новой категории<br>
Принимаемые параметры 
-name (string)<br>
-categories (array)<br>
Пример ответа<br>
{
    "code": true,
    "response": {
        "id": 11
    }
}
<br>
**PUT /api/items/edit/ID** Изменение товара<br>
Принимаемые параметры<br>
-name (string)<br>
-categories (array)<br>

**DELETE /api/items/remove/ID** Удаление товара<br>
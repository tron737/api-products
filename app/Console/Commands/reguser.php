<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Console\Command;

class reguser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reguser:new {name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Регистрация нового пользователя';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $password = $this->argument('password');
        $name = $this->argument('name');

        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ];

        $checkUser = User::where('email', '=', $email)->first();

        if ($checkUser) {
            $this->error("User already created");
            return;
        }

        $valid = $this->validator($data);

        if (!$valid) {
            $this->error("Data is not valid");
        }

        $reg = $this->create($data);

        if ($reg) {
            $this->info('User created');
        } else {
            $this->error("User don't created");
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\API\Categories;
use App\API\ItemCategory;
use App\API\Items;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Items $items, Request $request)
    {
        $category = $request->get('category');
        if (!$items->id) {
            if ($category) {
                $items = Categories::with(['items'])->where('id', '=', $category);
            } else {
                $items = Items::with(['categories']);
            }
        } else {
            $items = $items;
        }

        $items = $items->paginate(10);

        $result = [
            'code' => true,
            'response' => $items
        ];

        return response($result, $this->code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $name = $request->get('name');
        $categories = $request->get('categories');

        $result = [
            'code' => false,
            'response' => []
        ];

        if (!$name) {
            $this->code = 400;
            return response($result, $this->code);
        }

        $item = new Items();
        $item->name = $name;

        if ($item->save()) {
            $result['code'] = true;
            $result['response']['id'] = $item->id;

            if ($categories) {
                foreach ($categories as $category) {
                    $itemCategory = new ItemCategory();
                    $itemCategory->item_id = $item->id;
                    $itemCategory->category_id = $category;
                    $addItemCategory = $itemCategory->save();
                    if ($addItemCategory) {
                        $result['response']['categories'][] = $category;
                    }
                }
            }

            return response($result, $this->code);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\API\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Items $items)
    {
        $name = $request->get('name');
        $categories = $request->get('categories', []);

        $result = [
            'code' => false,
            'response' => [],
        ];

        if (!$name || !$items->id) {
            $this->code = 400;
            return response($result, $this->code);
        }

        $items->name = $name;

        if ($result['code'] = $items->save()) {
            if ($categories) {
                foreach ($items->categories as $itemCategory) {
                    $itemCategory->delete();
                }
                foreach ($categories as $category) {
                    $categoryItem = new ItemCategory();
                    $categoryItem->item_id = $items->id;
                    $categoryItem->category_id = $category;
                    $categoryItem->save();
                }
            }
        } else {
            $this->code = 400;
            return response($result, $this->code);
        }

        return response($result, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\API\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function destroy(Items $items)
    {

    }
}

<?php

namespace App\Http\Controllers\API;

use App\API\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Categories $categories)
    {
        if (!$categories->id) {
            $items = $categories::paginate(10);
        } else {
            $items = $categories;
        }

        $result = [
            'code' => true,
            'response' => $items
        ];

        return response($result, $this->code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $name = $request->get('name');

        $result = [
            'code' => false,
            'response' => [],
        ];

        if (!$name) {
            return response($result, $this->code);
        }

        $category = new Categories();

        $category->name = $name;

        if ($category->save()) {
            $result['code'] = true;
            $result['response']['id'] = $category->id;
        } else {
            $this->code = 401;
        }

        return response($result, $this->code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\API\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categories $categories)
    {
        $name = $request->get('name');

        $result = [
            'code' => false,
            'response' => [],
        ];

        if (!$name || !$categories->id) {
            $this->code = 400;
            return response($result, $this->code);
        }

        $categories->name = $name;

        if ($result['code'] = $categories->save()) {
            $result['response']['id'] = $categories->id;
            return response($result, $this->code);
        } else {
            $this->code = 400;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\API\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categories $categories)
    {
        $result = [
            'code' => false,
            'response' => [],
        ];

        if (!$categories->id) {
            $this->code = 400;
            return response($result, $this->code);
        }

        foreach ($categories->items as $item) {
            $item->delete();
        }

        $result['code'] = $categories->delete();

        return response($result, $this->code);
    }
}

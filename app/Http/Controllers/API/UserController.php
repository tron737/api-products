<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    public function auth(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        $result = [
            'code' => [],
            'response' => [],
        ];

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = Auth::user();

            $result['code'] = true;
            $result['response']['token'] = $user->createToken('API')->accessToken;
        } else {
            $result['code'] = false;
            $this->code = 401;
        }

        return response($result, $this->code);
    }
}

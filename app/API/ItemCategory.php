<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    protected $table = 'item_categories';
    public $timestamps = false;

    public function items()
    {
        return $this->hasMany('App\API\Items', 'id', 'item_id');
    }

}

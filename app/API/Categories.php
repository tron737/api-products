<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';

    public function items()
    {
        return $this->hasMany('App\API\ItemCategory', 'category_id', 'id');
    }
}

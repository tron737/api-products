<?php

namespace App\API;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = 'items';

    public function categories()
    {
        return $this->hasMany('App\API\ItemCategory', 'item_id', 'id')->with(['items']);
    }
}
